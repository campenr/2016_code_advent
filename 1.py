from math import sqrt

DIRECTION_MAPPING = {"R": 1, "L": -1}
ORIENTATION_MAPPING = ["N", "E", "S", "W"]

ROUTE = "R1, R3, L2, L5, L2, L1, R3, L4, R2, L2, L4, R2, L1, R1, L2, R3, L1, L4, R2, L5, R3, R4, L1, R2, L1, R3, L4, R5, L4, L5, R5, L3, R2, L3, L3, R1, R3, L4, R2, R5, L4, R1, L1, L1, R5, L2, R1, L2, R188, L5, L3, R5, R1, L2, L4, R3, R5, L3, R3, R45, L4, R4, R72, R2, R3, L1, R1, L1, L1, R192, L1, L1, L1, L4, R1, L2, L5, L3, R5, L3, R3, L4, L3, R1, R4, L2, R2, R3, L5, R3, L1, R1, R4, L2, L3, R1, R3, L4, L3, L4, L2, L2, R1, R3, L5, L1, R4, R2, L4, L1, R3, R3, R1, L5, L2, R4, R4, R2, R1, R5, R5, L4, L1, R5, R3, R4, R5, R3, L1, L2, L4, R1, R4, R5, L2, L3, R4, L4, R2, L2, L4, L2, R5, R1, R4, R3, R5, L4, L4, L5, L5, R3, R4, L1, L3, R2, L2, R1, L3, L5, R5, R5, R3, L4, L2, R4, R5, R1, R4, L3"
#ROUTE = "R4, R4"

def main():
    """Main program code."""
    
    # Follow route to destination or first repeated position
    destination = Position(0,0)   
    for instruction in ROUTE.split(", "):              
        status = destination.move_position(instruction)
        if status != 0:
            break
            
    # Calculate distance from origin (0, 0)
    print("Final Position: %s" % destination)
    print("Final Distance: %i" % int( sqrt((destination.position[0]) ** 2) + sqrt((destination.position[1]) ** 2)))
    
    return

class Position(object):
    """2D space positional object."""
        
    def __init__(self, x_pos, y_pos):
        self.position = (x_pos, y_pos)
        self.orientation = 0    # Start Facing North
        self.history = [(0, 0)]
        
    	
    def move_position(self, instruction):
        """Move position according to instruction."""        

        # Parse instruction
        direction, distance = Position.parse_instruction(instruction)
        
        # Update orientation
        new_orientation = (self.orientation + direction) % 4
        self.orientation = new_orientation
        
        # Update position
        while distance > 0:
            if self.orientation == 0: # North
                new_position = (self.position[0], self.position[1] - 1)
            elif self.orientation == 1: # East
                new_position = (self.position[0] - 1, self.position[1])
            elif self.orientation == 2: # South
                new_position = (self.position[0], self.position[1] + 1)
            elif self.orientation == 3: # West
                new_position = (self.position[0] + 1, self.position[1])
            
            self.position = new_position
        
            #######################
            # COMMENT THE FOLLOWING OUT TO GET ENDPOINT OF ROUTE INSTEAD OF FIRST CROSSOVER
            
            # Check if been here before otherwise update history and continue
            if self.position in self.history:
                return 1
            
            #            
            #######################
            
            self.history.append(self.position)
            distance -= 1
            
        return 0     
    
    @staticmethod    
    def parse_instruction(instruction):
        """Return positional and direction elements of instruction."""
             
        direction = DIRECTION_MAPPING[instruction[0]]
        distance = int(instruction[1:])
        
        return direction, distance
        
    def __repr__(self):
        return '<Orientation: %r; Position: %r>' % (ORIENTATION_MAPPING[self.orientation], self.position)
        
# Execute code
main()
