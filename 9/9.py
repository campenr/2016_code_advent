


def decompress(compressed_data):
    """Decode a file according to rules at http://adventofcode.com/2016/day/9.
    
    This function recursivley iterates over a compressed file, and calculates the length of substrings that contain
    no marker's, summing the returned lengths.
    """
    
    # init variables
    marker_start_stack = []
    marker_end_stack = []
    marker = ''
    marker_len = 0
    marker_copy = 0
    marker_last_index = 0
    decompressed_data_len = 0
    
    compressed_data_len = len(compressed_data)

    index = 0
    
    while (compressed_data_len > index):

        char = compressed_data[index]

        if char == '(':
            # save location to marker_start_stack
            marker_start_stack.append(index)

            
        elif char == ')':
            # save location to marker_end_stack
            # closing bracket means we are not inside a marker
            marker_end_stack.append(index)

            # get marker using the start and end indices in the respective stacks
            marker = compressed_data[(marker_start_stack[-1] + 1):marker_end_stack[-1]]
            
            # get marker parameters
            marker_len = int(marker.split('x')[0])
            marker_copy = int(marker.split('x')[1])
       
            # subset compressed data within the range of the end of the marker and marker_len              
            sub_data_start = marker_end_stack[-1] + 1
            sub_data_end = sub_data_start + marker_len
            
            sub_data = compressed_data[sub_data_start:sub_data_end]
            
            decompressed__sub_data_len = decompress(sub_data) * marker_copy           
            decompressed_data_len += decompressed__sub_data_len
            
            # set index to after sub_data
            index = sub_data_end -1 
            
        else:
            # character other than '(' or ')'
            if len(marker_start_stack) == len(marker_end_stack):
                
                # and we're not in a marker so we're an actual character we want to add
                decompressed_data_len += 1
                   
        # increment index
        index += 1

    return decompressed_data_len
        

if __name__ == '__main__':
    with open('2.input', 'r') as in_handle:
        len = decompress(in_handle.read().strip())
        print(len)
